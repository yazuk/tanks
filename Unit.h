#ifndef TANK_UNIT_H
#define TANK_UNIT_H

#include <iostream>
#include <list>
#include <string>
#include <uuid/uuid.h>


class Unit {

public:
    explicit Unit();
    Unit(const Unit &unit) = delete;
    virtual ~Unit();
    [[nodiscard]] virtual float speed() const;

private:

    float mSpeed = 0.0;
    std::string name;

protected:

};


#endif //TANK_UNIT_H
