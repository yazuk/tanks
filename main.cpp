#include <iostream>
#include <map>
#include <list>
#include <vector>
#include <array>
#include <cmath>

#include <GL/gl.h>
#include <GL/glut.h>

const int LEN_TEXT_OTKAZ = 16;
const int MAX_LEN_UUID = 40;

const float radius1 = 500.0;
const float radius2 = 250.0;
const float radius3 = 125.0;
const int countSegments = 90;

const int SIZE_BUFFER_TEXT = 256;
const int WIDTH_WINDOW = 1200;
const int HEIGHT_WINDOW = 1000;
static float xCurrent = 0.0;
static float yCurrent = 0.0;
static float centerX = 0.0;
static float centerY = 0.0;
char textOtkaz[LEN_TEXT_OTKAZ];

const char *radius1String = "500";
const char *radius2String = "250";
const char *radius3String = "125";
const float indentXStringScale = 40.0;

#include "Unit.h"
#include "Car.h"


//void renderBitmapString(
//        float x,
//        float y,
//        float z,
//        void *font,
//        const char *string) {
//
//    glRasterPos3f(x, y, z);
//    for (; *string != '\0'; string++)
//        glutBitmapCharacter(font, *string);
//}


//void drawCircle(float x0, float y0, float radius, int countSegments) {
//    glBegin(GL_LINE_LOOP);
//    for (int i = 0; i < countSegments; i++) {
//        float theta = 2.0f * M_PI * float(i) / float(countSegments);
//        float x = radius * cosf(theta);
//        float y = radius * sinf(theta);
//        glVertex2f(x + x0, y + y0);
//    }
//    glEnd();
//}


//void drawAirplane(float centerX, float centerY) {
//    glBegin(GL_LINES);
//    glVertex2f(centerX, centerY + 20);
//    glVertex2f(centerX, centerY - 20);
//    glVertex2f(centerX - 20, centerY);
//    glVertex2f(centerX + 20, centerY);
//    glEnd();
//}

//void reshape(int w, int h) {
//    glViewport(0, 0, w, h);
//
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    gluOrtho2D(0, w, 0, h);
//
//    glMatrixMode(GL_MODELVIEW);
//    glLoadIdentity();
//}

//void display() {
//
//    char buf[SIZE_BUFFER_TEXT];
//    centerX = glutGet(GLUT_WINDOW_WIDTH) / 2.0;
//    centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2.0;
//
//    glClearColor(0.0, 0.0, 0.0, 0.0);
//    glClear(GL_COLOR_BUFFER_BIT);
//    glPointSize(5);
//    glLineWidth(2);
//
//    glColor4f(1.0, 0.0, 0.0, 0.0);
//    drawAirplane(xCurrent, yCurrent);
//
//    glColor4f(0.65, 0.62, 0.62, 0.0);
//    drawCircle(centerX, centerY, radius1, countSegments);
//    drawCircle(centerX, centerY, radius2, countSegments);
//    drawCircle(centerX, centerY, radius3, countSegments);
//
//
//    snprintf(buf, SIZE_BUFFER_TEXT, "x:%.1f y:%.1f", xCurrent, yCurrent);
//    renderBitmapString(5, 90, 0, GLUT_BITMAP_HELVETICA_18, buf);
//    snprintf(buf, SIZE_BUFFER_TEXT, "Distance:%.1f",
//             sqrt(pow((centerX - xCurrent), 2) + (pow((centerY - yCurrent), 2))));
//    renderBitmapString(5, 60, 0, GLUT_BITMAP_HELVETICA_18, buf);
//    float azimuth = atan2((yCurrent - centerY), (xCurrent - centerX)) / M_PI * 180.0;
////    azimuth = (azimuth < 0.0) ? azimuth + 360.0 : azimuth;
//    snprintf(buf, SIZE_BUFFER_TEXT, "Azimuth:%.5f", azimuth);
//    renderBitmapString(5, 30, 0, GLUT_BITMAP_HELVETICA_18, buf);
//    renderBitmapString(1100, 950, 0, GLUT_BITMAP_HELVETICA_18, textOtkaz);
//
//    renderBitmapString(centerX - radius1 - indentXStringScale, centerY, 0, GLUT_BITMAP_HELVETICA_18, radius1String);
//    renderBitmapString(centerX - radius2 - indentXStringScale, centerY, 0, GLUT_BITMAP_HELVETICA_18, radius2String);
//    renderBitmapString(centerX - radius3 - indentXStringScale, centerY, 0, GLUT_BITMAP_HELVETICA_18, radius3String);
//
//    glutSwapBuffers();
//}

//void keyboardEvent(unsigned char key, int a, int b) {
//
//
//    fprintf(stdout, "Hello World\n");
//    switch (key) {
//        case GLUT_KEY_UP:
//            yCurrent += 1.0;
//            glutPostRedisplay();
//            break;
//        case GLUT_KEY_DOWN:
//            yCurrent -= 1.0;
//            glutPostRedisplay();
//            break;
//        case GLUT_KEY_LEFT:
//            xCurrent -= 1.0;
//            glutPostRedisplay();
//            break;
//        case GLUT_KEY_RIGHT:
//            xCurrent += 1.0;
//            glutPostRedisplay();
//            break;
//        default:
//            break;
//    }
//}


int main(int argc, char **argv) {

    Unit unit;
    Car car;

    uuid_t id;
    char str[MAX_LEN_UUID];
    uuid_generate(id);
    uuid_unparse(id, str);
//    mName = str;


//    glutInit(&argc, argv);
//
//    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
//
//    glutInitWindowSize(WIDTH_WINDOW, HEIGHT_WINDOW);
//    glutCreateWindow("U2");
//
//    glutKeyboardFunc(keyboardEvent);
//    glutReshapeFunc(reshape);
//    glutDisplayFunc(display);
//
//    xCurrent = glutGet(GLUT_WINDOW_WIDTH) / 2.0;
//    yCurrent = glutGet(GLUT_WINDOW_HEIGHT) / 2.0;
//
//    glutMainLoop();


    return 0;
}
