#include "LAP_CLIENTS.h"
#include "ui_LAP_CLIENTS.h"

#include "lap_client.h"

#include <QSqlQuery>
#include <QMessageBox>
#include <QSqlError>
#include <QInputDialog>
#include <QString>
#include <QtXml>
#include "audit.h"
#include "contracts.h"
#include "nets.h"
#include "../../simplecrypt.h"
#include <memory>
#include "../Info/lap_info.h"

#include "mversion.h"

extern "C" Q_DECL_EXPORT LAP_AbstractModule *
createModule(QWidget
*parent) {
return new
LAP_CLIENTS(parent);
}

extern "C" Q_DECL_EXPORT void getRulisModuleVersion(QString &ver, QString &dt) {
    ver = RULIS_VERSION;
    dt = RULIS_COMPILE_DATE;
    dt += " ";
    dt += RULIS_COMPILE_TIME;
}

void LAP_CLIENTS::install() {}

void LAP_CLIENTS::remove() {}

void LAP_CLIENTS::upgrade() {}

void LAP_CLIENTS::setParameters(const QString &params) {
    ro = (params == "readonly");

    if (ro) {
        ui->btnAdd->setEnabled(false);
        ui->btnDel->setEnabled(false);
        ui->btnAudit->setEnabled(false);
    }

    LoadData();
    connect(ui->tblClients, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(Show_Client())); //клиент
    connect(ui->btnNets, SIGNAL(clicked()), this, SLOT(shownets()));
}

LAP_CLIENTS::LAP_CLIENTS(QWidget * parent)
:

LAP_AbstractModule (parent),
ui(new Ui::LAP_CLIENTS) {
    ui->setupUi(this);
    moduleName = "LAP_CLIENTS";
    this->setWindowTitle(this->windowTitle() + " " + RULIS_VERSION);

    ui->tblClients->verticalHeader()->setDefaultSectionSize(20);
    //тут наполняем cb_category
    ui->cbCateg->addItem(tr("Все"), "-1");
    QSqlQuery query;
    query.exec("select id, category from d_clientcategories  order by category");
    while (query.next())
        ui->cbCateg->addItem(query.value(1).toString(), query.value(0));
    //тут наполняем cb_category


    //---------------слоты--------------------
    connect(ui->cbCateg, SIGNAL(currentIndexChanged(int)), this, SLOT(LoadData())); //комбобокс
    connect(ui->btnAdd, SIGNAL(clicked()), this, SLOT(AddClient())); //добавить клиента
    connect(ui->btnDel, SIGNAL(clicked()), this, SLOT(DelClient())); //удалить клиента
    connect(ui->btnAudit, SIGNAL(clicked()), this, SLOT(Show_audit())); //аудит
    connect(ui->btnPanelsList, SIGNAL(clicked()), this, SLOT(Show_PanelsList())); // список панелей
    connect(ui->btnContracts, SIGNAL(clicked()),
            this, SLOT(showContracts()));

    connect(ui->btnKey, SIGNAL(clicked(bool)),
            this, SLOT(key()));

    connect(ui->txtSearch, SIGNAL(returnPressed()),
            this, SLOT(search()));

    //---------------слоты--------------------
}

LAP_CLIENTS::~LAP_CLIENTS() {
    delete ui;
}

//-----------------------------загрузка данных в датагрид
void LAP_CLIENTS::LoadData() {
    //берём значение из комбобокса
    //выбираем на основе этого значения какой запрос делать.
    QString clientcat = ui->cbCateg->itemData(ui->cbCateg->currentIndex()).toString();
    QString sql = "";

    sql = "select p.id,p.clientcode,p.clientname,c.category,p.by_user, p.logo, p.active, p.useprice, p.adress from clients p left join d_clientcategories c on p.catclientid=c.id ";

    ui->tblClients->clearColumns();
    ui->tblClients->addColumn("id", "id", tr("№"), true, true);
    ui->tblClients->addColumn("clientcode", "clientcode", tr("Код\nклиента"), true, false, "TEXTEDIT");
    ui->tblClients->addColumn("clientname", "clientname", tr("Название клиента"), true, false, "TEXTEDIT");
    ui->tblClients->addColumn("category", "category", tr("Категория"), true, false, "TEXTEDIT");
    ui->tblClients->addColumn("by_user", "by_user", tr("Создано"), true, false, "TEXTEDIT");


    bool price = (LAP_Info::getCurrentRole()).startsWith(tr("Админи"));
    if ((LAP_Info::getCurrentRole()).startsWith(tr("Админи")) && (LAP_Info::getCurrentRole()).endsWith(tr("тратор")));
    {
        ui->tblClients->addColumn("logo", "logo", tr("Лого-\nтип"), false, false, "CHECKBOXA");
        ui->tblClients->addColumn("active", "active", tr("Акти-\nвный"), false, false, "CHECKBOXA");
        ui->tblClients->addColumn("useprice", "useprice", tr("Прайс"), false, false, "CHECKBOXA");
    }
    else
    {
        ui->tblClients->addColumn("logo", "logo", tr("Логотип"), true, false, "CHECKBOXA");
        ui->tblClients->addColumn("active", "active", tr("Активный"), true, false, "CHECKBOXA");
        ui->tblClients->addColumn("useprice", "useprice", tr("Учитывать прайс"), false, true, "CHECKBOXA");
    }

    ui->tblClients->addColumn("adress", "adress", tr("Адрес"), true, false, "TEXTEDIT");

    if (clientcat == "-1")
        ui->tblClients->setQuery(sql + "  " + " order by 2", "clients", "id", "p.id", false);
    else
        ui->tblClients->setQuery(sql + " where p.catclientid =" + clientcat + " " + " order by 2", "clients", "id",
                                 "p.id", false);
    ui->tblClients->setRefreshQuery(sql);

    ui->tblClients->setColumnWidth(1, 80);
    ui->tblClients->setColumnWidth(2, 400);

    ui->tblClients->setColumnWidth(5, 50);
    ui->tblClients->setColumnWidth(6, 50);
    ui->tblClients->setColumnWidth(7, 50);

    ui->tblClients->horizontalHeader()->setStretchLastSection(true);
    ui->tblClients->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tblClients->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->tblClients->horizontalHeader()->setHighlightSections(false);

}

//-----------------------------загрузка данных в датагрид
//-----------------------------добавление записи в таблицу клиентов---------------------------------------------------------------------------------
void LAP_CLIENTS::AddClient() {
    bool ok;
    QString text = QInputDialog::getText(this, tr("Введите Код клиента"),
                                         tr("Код:"), QLineEdit::Normal,
                                         "", &ok).trimmed();
    if (ok && !text.isEmpty()) {

        QSqlQuery query;
        query.exec("select clientcode from clients where clientcode = '" + text + "'");
        if (!query.next()) {
            query.clear();
            QString user = LAP_Info::getCurrentUser();
            QString sqlt = "insert into  clients(clientcode,by_user) values(?,'" + user + "')";

            LAP_Info::setWriteOnlyTransaction();
            query.prepare(sqlt);
            query.addBindValue(text);
            if (!query.exec()) {
                QMessageBox::critical(this, tr("Ошибка"),
                                      tr("При добавлении клиента произошла ошибка.\n%1").arg(query.lastError().text()));
                return;
            }
            LoadData();
            int row = ui->tblClients->getRowByField("clientcode", text);
            ui->tblClients->selectRow(row);
        } else
            QMessageBox::critical(this, tr("Ошибка"), tr("Такое код уже существует в базе."));
    }
}

//-------------------------------------добавление записи в таблицу клиентов----------------------------------------------------------------------------
//----------------------------удаление записи из таблицу клиентов-------------------------------------------------------------------------------------
void LAP_CLIENTS::DelClient() {
    if (!(ui->tblClients->model() && ui->tblClients->model()->rowCount() > 0))
        return;

    QString id = ui->tblClients->getCurrentData("ID").toString();     //получаем id удаляемого элемента
    QMessageBox::StandardButton ret = QMessageBox::question(this, \
                   tr("Вопрос"), tr("Вы уверены, что хотите удалить клиента?"), \
                   QMessageBox::Yes | QMessageBox::No);
    if (ret == QMessageBox::Yes) {

        QSqlQuery query;
        //------------проверяем наличие образцов
        query.exec("select first 5 id from folders where clientid = " + id + "");
        if (query.next()) {
            QMessageBox::critical(this, tr("Ошибка"), tr("У клиента есть образцы"));
            return;
        }
        //------------проверяем наличие образцов


        query.clear();
        QDomDocument *doc = new QDomDocument;

        QDomElement root = doc->createElement("Clients");
        doc->appendChild(root);

        QDomElement panel = doc->createElement("Client");
        root.appendChild(panel);

        QDomElement tag = doc->createElement("id");
        panel.appendChild(tag);

        QDomText text = doc->createTextNode(id);
        tag.appendChild(text);

        query.exec(
                "select  p.id,p.clientcode,c.category,p.by_user,p.clientname from clients p left join d_clientcategories c on p.catclientid=c.id where p.id =" +
                id);
        query.next();


        //clientcode
        tag = doc->createElement("id");
        panel.appendChild(tag);
        text = doc->createTextNode(query.value(0).toString());
        tag.appendChild(text);

        //clientcode
        tag = doc->createElement("clientcode");
        panel.appendChild(tag);
        text = doc->createTextNode(query.value(1).toString());
        tag.appendChild(text);

        //category
        tag = doc->createElement("category");
        panel.appendChild(tag);
        text = doc->createTextNode(query.value(2).toString());
        tag.appendChild(text);

        //by_user
        tag = doc->createElement("by_user");
        panel.appendChild(tag);
        text = doc->createTextNode(query.value(3).toString());
        tag.appendChild(text);

        //clientname
        tag = doc->createElement("clientname");
        panel.appendChild(tag);
        text = doc->createTextNode(query.value(4).toString());
        tag.appendChild(text);


        QString audit_after = doc->toString(1).replace("<!DOCTYPE Clients>", "<?xml version=\"1.0\" ?>");

        query.clear();
        LAP_Info::setWriteOnlyTransaction();

        query.prepare(
                "insert into audit(tablename, table_id, usernam, logdate, audit_before ) values(?,?,?, current_timestamp, ?)");
        query.addBindValue("Clients");
        query.addBindValue(id);
        query.addBindValue(LAP_Info::getCurrentUser());
        query.addBindValue(audit_after);
        query.exec();

        delete doc;

        query.clear();
        LAP_Info::setWriteOnlyTransaction();
        if (!query.exec("delete from clients where id = " + id)) {
            QMessageBox::critical(this, tr("Ошибка"),
                                  tr("При удалении записей из таблицы Clients произошла ошибка.\n%1").arg(
                                          query.lastError().text()));
            return;
        } else
            LoadData();
    }
}
//----------------------------удаление записи из таблицу клиентов------------------------------------------------------------------------------------

//-----------------форма аудита
void LAP_CLIENTS::Show_audit() {
    Audit *aud = new Audit(this);
    aud->exec();
}

//-----------------форма аудита
//------------------вызов формы client--------------------------------------------------------------
void LAP_CLIENTS::Show_Client() {
    if (!(ui->tblClients->model() && ui->tblClients->model()->rowCount() > 0)) return;
    try {
        QString _id = ui->tblClients->getCurrentData("ID").toString();
        // QString _idtestcode = ui->tblTests->getCurrentData("ID").toString();
        //QMessageBox::critical(this, tr("Ошибка"),_id );
        LAP_CLIENT add(this, _id, ro);
        if (add.exec()) {

            int row = ui->tblClients->getRowByField("id", _id);
            ui->tblClients->selectRow(row);
        }
    }
    catch (transaction_error) {
        QMessageBox::warning(this, tr("Ошибка"),
                             tr("В настоящее время запись находится в состоянии редактирования другим пользователем."));
    }
}

//----------------- PDF со списком панелей для данного клиента
void LAP_CLIENTS::Show_PanelsList() {
    QString _id = ui->tblClients->getCurrentData("ID").toString();
    QString reportServer = LAP_Info::getReportServer().trimmed();

    if (reportServer.isEmpty())
        reportServer = LAP_Info::GetProperty("report-server");

    //в ярлыках указывается адрес до report.php, то есть
    //https://ip-address/nreports/report.php
    //именно через этот печатаются результаты

    if (reportServer.endsWith("report.php"))
        reportServer = reportServer.left(reportServer.length() - 10);
    else {
        if (!reportServer.endsWith("/"))
            reportServer += "/";

        reportServer += "nreports/";
    }

    QUrl url(reportServer + "clientpanels.php?clientid=" + _id);

    qDebug() << "Show_PanelsList";
    qDebug() << "client ID: " << _id;
    qDebug() << "url: " << url.toString();
    qDebug() << "getReport answer: " << LAP_Info::getReport(url);
}

void LAP_CLIENTS::showContracts() {
    QModelIndex ind = ui->tblClients->currentIndex();
    if (!ind.isValid())
        return;

    QString id = ui->tblClients->model()->index(ind.row(), 0).data().toString();
    Contracts *a = new Contracts(this, id);
    a->exec();
    delete a;

}

void LAP_CLIENTS::showNets() {
    Nets *a = new Nets(this);
    if (a->exec())
        delete a;
}

void LAP_CLIENTS::search() {
    QString pattern = ui->txtSearch->text().trimmed().toUpper();
    if (!start.isValid()) {
        start = ui->tblClients->currentIndex();
        start = ui->tblClients->model()->index(start.row(), 1);
    }

    QModelIndexList inds = ui->tblClients->model()->match(start, Qt::DisplayRole, pattern, 1,
                                                          Qt::MatchContains | Qt::MatchWrap);
    if (inds.size() > 0) {
        start = inds[0];

        ui->tblClients->setCurrentIndex(start);
        ui->tblClients->scrollTo(start);

        ui->tblClients->selectRow(start.row());
        start = ui->tblClients->model()->index(inds[0].row() + 1, inds[0].column());
    } else {
        if (ui->tblClients->model()->canFetchMore(QModelIndex())) {
            while (ui->tblClients->model()->canFetchMore(QModelIndex()))
                ui->tblClients->model()->fetchMore(QModelIndex());

            search();
        } else {
            //поиск по второй колонке
            if (start.column() == 1) {
                start = ui->tblClients->model()->index(start.row(), 2);
                QModelIndexList inds = ui->tblClients->model()->match(start, Qt::DisplayRole, pattern, 1,
                                                                      Qt::MatchContains | Qt::MatchWrap);
                if (inds.size() > 0) {
                    start = inds[0];

                    ui->tblClients->setCurrentIndex(start);
                    ui->tblClients->scrollTo(start);
                    ui->tblClients->selectRow(start.row());

                    start = ui->tblClients->model()->index(inds[0].row() + 1, inds[0].column());
                } else
                    start = ui->tblClients->model()->index(start.row(), 1);

            } else {
                start = ui->tblClients->model()->index(start.row(), 1);
                QModelIndexList inds = ui->tblClients->model()->match(start, Qt::DisplayRole, pattern, 1,
                                                                      Qt::MatchContains | Qt::MatchWrap);
                if (inds.size() > 0) {
                    start = inds[0];

                    ui->tblClients->setCurrentIndex(start);
                    ui->tblClients->scrollTo(start);

                    ui->tblClients->selectRow(start.row());

                    start = ui->tblClients->model()->index(inds[0].row() + 1, inds[0].column());
                }
            }
        }
    }

}

void LAP_CLIENTS::key() {
    QModelIndex ind = ui->tblClients->currentIndex();
    if (!ind.isValid())
        return;

    QString id = ui->tblClients->model()->index(ind.row(), 0).data().toString();

    QSqlQuery query;
    query.prepare("select id, clientcode || ' - ' || clientname from clients where id = ?");
    query.addBindValue(id);

    if (query.exec() && query.next()) {
        QString id = query.value(0).toString();
        QString key = "5b2e6d61-1bea-4c8f-811e-b95a946a7e46";
        QDate date = QDate::currentDate().addDays(365);
        QString dept = query.value(1).toString();

        QString port = "1309";
        QString host = "127.0.0.25";

        QString data = "<settingslist>"
                       "<settings>"
                       "<uid>" + id + "</uid>"
                                      "<key>" + key + "</key>"
                                                      "<server>" + host + "</server>"
                                                                          "<port>" + port + "</port>"
                                                                                            "<enddate>" +
                       date.toString("yyyy/MM/dd") + "</enddate>"
                                                     "<dept>" + dept + "</dept>"
                                                                       "</settings>"
                                                                       "</settingslist>";

        qDebug() << data;

        std::shared_ptr<SimpleCrypt> crypt(new SimpleCrypt(Q_UINT64_C(0x4c5affadacb9f023)));
        QString crypt_text = crypt->encryptToString(data);

        QString lf = LAP_Info::getHomePath() + "/settings.xml";
        QFile file(lf);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QMessageBox::critical(this, tr("Ошибка"),
                                  tr("Не могу создать файл с настройками, проверьте правильность файла, разрешений и пути.\n%1").arg(
                                          lf));
            return;
        }

        file.write(crypt_text.toUtf8());
        file.close();

        QMessageBox::information(this, tr("Ключ создан"),
                                 tr("Файл с ключом settings.xml успешно создан в домашней директории програмы."));
    }
}

