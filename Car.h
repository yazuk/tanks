#ifndef TANK_CAR_H
#define TANK_CAR_H
#include "Unit.h"


class Car : public Unit {

public:

    explicit Car();

    virtual ~Car();

private:

    float price;

};


#endif //TANK_CAR_H
